$(document).ready(function(){
    var mMenu = $('.mobile-menu'),
        mIcon = $('.mobile-icon'),
        wrap = $('.js-page-wrap');
    mIcon.click(function(){
        if(mMenu.hasClass('js-hide'))
        {
            mMenu.removeClass('js-hide').animate({'width': '80%'});
        }
        else
        {
            mMenu.addClass('js-hide').animate({'width':'0%'});
        }

        if (mIcon.hasClass('close-icon'))
            {mIcon.removeClass('close-icon');
            wrap.css({'position':'static'}).animate({'margin-left':'0'})
            ;}

        else
            {
            mIcon.addClass('close-icon');
            wrap.css({'position':'fixed', 'overflow':'hidden'}).animate({'margin-left':'80%'})
            ;}
    });


    $('#city-dropdown').click(function () {
        $('#city-content').slideToggle();
        $(this).find('.icon-drop').toggleClass('_open');
    });

    $('.header__bottom').on('click', '.search-fake', function() {
        $('#icon-search-fake').toggle();
        $('#icon-close-search').toggleClass('_hide');
        $('#search-panel').slideToggle();
    });


    $('.video-layout-slide').slick({
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: true,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slick: true,
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },

            {
                breakpoint: 768,
                settings: 'unslick',
                arrows: false
            }
        ]
    });

    function loadMore (item) {
        const cont = $(item);
        const i = cont.find('[data-video-item]');
        const btn = cont.find('.btn-load-more');

        if (i.length <= 3) {
            btn.hide();
        } else {
            cont.on('click', btn, function () {
                i.toggleClass('_show');
                btn.fadeOut();
            });
        }
    }
    loadMore('#Inspiration');
    loadMore('#Craft-education');
    loadMore('#Step-by-step');
});
